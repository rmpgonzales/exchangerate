create table exchangerate.eur_exchange_rate
(
    id int primary key auto_increment,
    date date,
    USD decimal(10,4),
    GBP decimal(10,4),
    CHF decimal(10,4),
    AUD decimal(10,4),
    CAD decimal(10,4)
);

create table exchangerate.usd_exchange_rate
(
    id int primary key auto_increment,
    date date,
    EUR decimal(10,4),
    GBP decimal(10,4),
    CHF decimal(10,4),
    AUD decimal(10,4),
    CAD decimal(10,4)
);

create table exchangerate.php_exchange_rate
(
    id int primary key auto_increment,
    date date,
    EUR decimal(10,4),
    USD decimal(10,4),
    GBP decimal(10,4),
    CHF decimal(10,4),
    AUD decimal(10,4),
    CAD decimal(10,4)
);