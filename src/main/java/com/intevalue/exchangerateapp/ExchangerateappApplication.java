package com.intevalue.exchangerateapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ExchangerateappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExchangerateappApplication.class, args);
	}

}
