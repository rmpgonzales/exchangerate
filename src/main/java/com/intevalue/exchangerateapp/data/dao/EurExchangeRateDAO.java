package com.intevalue.exchangerateapp.data.dao;

import com.intevalue.exchangerateapp.data.persistence.EurExchangeRateEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EurExchangeRateDAO extends JpaRepository<EurExchangeRateEntity, Integer>{
    
}
