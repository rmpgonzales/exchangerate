package com.intevalue.exchangerateapp.data.dao;

import com.intevalue.exchangerateapp.data.persistence.UsdExchangeRateEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsdExchangeRateDAO extends JpaRepository<UsdExchangeRateEntity, Integer>{
    
}
