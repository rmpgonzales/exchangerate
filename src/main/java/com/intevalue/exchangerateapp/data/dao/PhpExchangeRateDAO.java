package com.intevalue.exchangerateapp.data.dao;

import com.intevalue.exchangerateapp.data.persistence.PhpExchangeRateEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PhpExchangeRateDAO extends JpaRepository<PhpExchangeRateEntity, Integer> {

}
