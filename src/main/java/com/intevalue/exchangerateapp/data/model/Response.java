package com.intevalue.exchangerateapp.data.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Response {
    @JsonProperty
    private Rates rates;

    @JsonProperty
    private String base;

    @JsonProperty
    private LocalDate date;

    @Getter
    @Setter
    @ToString
    public static class Rates {
        @JsonProperty
        private BigDecimal USD;
        @JsonProperty
        private BigDecimal PHP;
        @JsonProperty
        private BigDecimal EUR;
        @JsonProperty
        private BigDecimal GBP;
        @JsonProperty
        private BigDecimal CHF;
        @JsonProperty
        private BigDecimal AUD;
        @JsonProperty
        private BigDecimal CAD;
    }
}
