package com.intevalue.exchangerateapp.data.persistence;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "php_exchange_rate", schema = "exchangerate")
public class PhpExchangeRateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private LocalDateTime date;

    @Column(name = "EUR")
    private BigDecimal eur;

    @Column(name = "USD")
    private BigDecimal usd;

    @Column(name = "GBP")
    private BigDecimal gbp;

    @Column(name = "CHF")
    private BigDecimal chf;
    
    @Column(name = "AUD")
    private BigDecimal aud;

    @Column(name = "CAD")
    private BigDecimal cad;
}
