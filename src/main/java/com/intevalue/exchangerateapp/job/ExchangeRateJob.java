package com.intevalue.exchangerateapp.job;

import java.time.LocalDateTime;

import com.intevalue.exchangerateapp.data.dao.EurExchangeRateDAO;
import com.intevalue.exchangerateapp.data.dao.PhpExchangeRateDAO;
import com.intevalue.exchangerateapp.data.dao.UsdExchangeRateDAO;
import com.intevalue.exchangerateapp.data.model.Response;
import com.intevalue.exchangerateapp.data.persistence.EurExchangeRateEntity;
import com.intevalue.exchangerateapp.data.persistence.PhpExchangeRateEntity;
import com.intevalue.exchangerateapp.data.persistence.UsdExchangeRateEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ExchangeRateJob {

    private static final String EUR = "EUR";
    private static final String USD = "USD";
    private static final String PHP = "PHP";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EurExchangeRateDAO eurDAO;

    @Autowired
    private UsdExchangeRateDAO usdDAO;

    @Autowired
    private PhpExchangeRateDAO phpDAO;

    @Scheduled(fixedDelayString = "36000000")
    public void getExchangeRate() {
        String[] currencies = { "EUR", "USD", "PHP" };
        for (String currency : currencies) {
            UriComponentsBuilder uriBuilder = UriComponentsBuilder
                    .fromHttpUrl("https://api.exchangeratesapi.io/latest");
            uriBuilder.queryParam("base", currency);
            HttpMethod method = HttpMethod.GET;
            HttpEntity<String> requestEntity = new HttpEntity<>(null, null);
            ResponseEntity<Response> response = restTemplate.exchange(uriBuilder.build().toUriString(), method,
                    requestEntity, Response.class);

            if (EUR.compareToIgnoreCase(currency) == 0) {
                saveEurRate(response.getBody());
            } else if (USD.compareToIgnoreCase(currency) == 0) {
                saveUsdRate(response.getBody());
            } else if (PHP.compareToIgnoreCase(currency) == 0) {
                savePhpRate(response.getBody());
            }
        }
    }

    private void saveEurRate(Response response) {
        EurExchangeRateEntity entity = new EurExchangeRateEntity();
        entity.setAud(response.getRates().getAUD());
        entity.setCad(response.getRates().getCAD());
        entity.setChf(response.getRates().getCHF());
        entity.setGbp(response.getRates().getGBP());
        entity.setUsd(response.getRates().getUSD());
        entity.setDate(LocalDateTime.now());
        eurDAO.save(entity);
    }

    private void saveUsdRate(Response response) {
        UsdExchangeRateEntity entity = new UsdExchangeRateEntity();
        entity.setAud(response.getRates().getAUD());
        entity.setCad(response.getRates().getCAD());
        entity.setChf(response.getRates().getCHF());
        entity.setGbp(response.getRates().getGBP());
        entity.setEur(response.getRates().getEUR());
        entity.setDate(LocalDateTime.now());
        usdDAO.save(entity);
    }

    private void savePhpRate(Response response) {
        PhpExchangeRateEntity entity = new PhpExchangeRateEntity();
        entity.setAud(response.getRates().getAUD());
        entity.setCad(response.getRates().getCAD());
        entity.setChf(response.getRates().getCHF());
        entity.setGbp(response.getRates().getGBP());
        entity.setEur(response.getRates().getEUR());
        entity.setUsd(response.getRates().getUSD());
        entity.setDate(LocalDateTime.now());
        phpDAO.save(entity);
    }
}
